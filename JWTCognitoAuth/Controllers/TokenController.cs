﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

using Microsoft.AspNetCore.Authorization;

namespace JWTCognitoAuth.Controllers
{
    [Produces("application/json")]
    [Route("api/Token")]
    [Authorize]
    public class TokenController : Controller
    {
        // GET: api/Token
        [HttpGet]
        public string Get()
        {

            string cognitousername = User.Claims.Where(c => c.Type == "cognito:username").First().Value;
            string familyname = User.Claims.Where(c => c.Type == "family_name").First().Value;
            string givenname = User.Claims.Where(c => c.Type == "given_name").First().Value;
            string sub = User.Claims.Where(c => c.Type == "sub").First().Value;
            string exp = User.Claims.Where(c => c.Type == "exp").First().Value;



            return cognitousername + ", Name: " + givenname + ", Last Name: " + familyname +" , Your uuid is: " + sub + ", exp: " + (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddSeconds(Double.Parse(exp)).ToString();
        }
    }
}
